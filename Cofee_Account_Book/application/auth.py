
from flask import Blueprint

auth = Blueprint('auth',__name__);

@auth.route('/add')
def add_to_db():
    return 'add';

@auth.route('/delete/<int:index>')
def delete_db_item(index):
    return 'Delete';

@auth.route('/read')
def read_all_from_db():
    return 'Read';

@auth.route('/update')
def update_db_item():
    return 'update';
