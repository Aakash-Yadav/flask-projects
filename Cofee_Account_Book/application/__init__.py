

from flask import Flask
from os import urandom,path,listdir
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
DB_NAME = 'todo.db';
BASE_DIR = path.abspath(path.dirname(__file__))

def create_application():

    app = Flask(__name__);

    app.config['SECRET_KEY'] = '%s'%(urandom(100));
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+path.join(BASE_DIR,DB_NAME);
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0;

    db.init_app(app);

    
    from .views import views

    app.register_blueprint(views,url_prefix="/");

    from .auth import auth
    
    app.register_blueprint(auth,url_prefix='/');

    create_db(app);

    return app;


def create_db(app):
    
    if not path.exists('application/'+DB_NAME):
        db.create_all(app=app);
    return 1 


            

