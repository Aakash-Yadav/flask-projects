


from WebApp import app,db 
from flask import render_template, redirect,request,url_for,abort,flash
from flask_login import login_user,login_required,logout_user
from WebApp.forms import Register_form,Login
from WebApp.models import User



@app.route('/')
def home():
    return render_template('home.html')





if __name__ == '__main__':
    app.run(debug=1)
