
from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField
from wtforms.validators import DataRequired,Email,EqualTo
from wtforms import ValidationError
from .models import User

class Login(FlaskForm):

    email = StringField('Email',validators=[DataRequired(),Email()])

    password = PasswordField('Password',validators=[DataRequired()])

    submit = SubmitField("Login")

class Register_form(FlaskForm):

    email = StringField('Email',validators=[DataRequired(),Email()])

    username = StringField('Username',validators=[DataRequired()])

    password = PasswordField("PAssword",validators=[DataRequired(),EqualTo('pass_conform',message="Password Must Match")])

    pass_conform = PasswordField("Conform Password",validators=[DataRequired()])

    submit = SubmitField("Singup")

    def check_email(self,field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email used ")

    def check_user_name(self,field):
        if User.query.filter_by(username=field.data):
            raise ValidationError("Username is taken!")

