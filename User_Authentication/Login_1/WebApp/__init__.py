
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path,urandom 
from flask_migrate import Migrate
from flask_login import LoginManager


login_manager = LoginManager()

app = Flask(__name__);

app.config['SECRET_KEY'] = '%s'%(urandom(100));

base_dir = path.abspath(path.dirname(__file__));

DB_NAME = 'login.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+path.join(base_dir,DB_NAME);
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0;

db = SQLAlchemy(app);

Migrate(app);

login_manager.init_app(app);

login_manager.login_view = 'login';







