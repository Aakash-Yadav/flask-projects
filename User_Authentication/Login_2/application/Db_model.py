
from . import db
from flask_login import UserMixin;
from flask_bcrypt import generate_password_hash
from datetime import datetime 

class SCHOOL(db.Model,UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=False)
    password =  db.Column(db.String(150), unique=True, nullable=False)
    date_time = db.Column(db.String(15),default="%s"%(datetime.now().date()));

    def __init__(self,username,email,password):
        self.username = username;
        self.email = email ;
        self.password = generate_password_hash(password);

    def __repr__(self):
        return "%s"%(self.username)
