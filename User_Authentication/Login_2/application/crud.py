

from . import db
from .Db_model import SCHOOL 
from flask_bcrypt import check_password_hash,generate_password_hash

def add_to_db(name,email,password):
    
    Name = SCHOOL.query.filter_by(username=name).first()
    Email = SCHOOL.query.filter_by(email=email).first()

    if Name:
        return "username already taken";
    elif Email:
        return "email already registered";
    else:
        new = SCHOOL(username=name,email=email,password=password);
        db.session.add(new);
        db.session.commit();
        return 1;

def Delete_from_db(username):
    delete_data = SCHOOL.query.filter_by(username=username).first()
    if delete_data:
        db.session.delete(delete_data.id);
        db.session.commit()
        return 1;
    else:
        return 0;

def read_all_from_db():
    data = SCHOOL.query.all()
    return data 

def search_data_in_db(email,password_):

    data = SCHOOL.query.filter_by(email=email).first();
    if data:
        if check_password_hash(data.password,password_):
            return (data, 1)
        else:
            return ( "Email exist but password is invalid",0)
    else:
        return ("email doesn't exist",0)
