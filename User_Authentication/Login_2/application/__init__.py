
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path,urandom
from flask_login import LoginManager

BASE_DIR = path.abspath(path.dirname(__file__))
DB_NAME = 'auth.db';

db = SQLAlchemy()

def create_app():

    app = Flask(__name__);

    app.config['SECRET_KEY'] = '%s'%(urandom(100));
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+path.join(BASE_DIR,DB_NAME);
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0;
    
    db.init_app(app);


    from .views import views;
    from .login_view import login_auth;

    app.register_blueprint(views,url_prefix='/');
    app.register_blueprint(login_auth,url_prefix='/');

    from .Db_model import SCHOOL;
    
    create_db(app)

    login_manager = LoginManager()
    login_manager.login_view = 'login_auth.login';
    login_manager.init_app(app)
    
    @login_manager.user_loader
    def load_user(user_id):
        print(user_id)
        return SCHOOL.query.get(int(user_id))

    return app;

def create_db(app):
    if not path.exists('application/'+DB_NAME):
        db.create_all(app=app);
    else:
        pass 
    return 1

