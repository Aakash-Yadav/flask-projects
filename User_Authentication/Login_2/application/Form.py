
from flask_wtf import FlaskForm 
from wtforms import StringField,SubmitField,EmailField,PasswordField
from wtforms.validators import DataRequired,Length,ValidationError,Email,EqualTo

from .Db_model import SCHOOL;

class Register(FlaskForm):

    username = StringField(validators=[DataRequired(),Length(min=4,max=20)],
            render_kw={'placeholder':"Username"})

    email = EmailField(validators=[DataRequired(),Email()],
            render_kw={'placeholder':"Email address"})

    password = PasswordField("PAssword",validators=[DataRequired(),
        EqualTo('pass_conform',message="Password Must Match"),Length(min=8,max=20)],
        render_kw={'placeholder':'Password'});

    pass_conform = PasswordField("Conform Password",validators=[DataRequired()
        ,Length(min=8,max=20)],
        render_kw={'placeholder':"confirm password"})

    submit = SubmitField('SingUp')

class Login(FlaskForm):

    email = StringField('Email',validators=[DataRequired(),Email()],
            render_kw={'placeholder':'Email address'})

    password = PasswordField('Password',validators=[DataRequired()],
            render_kw={'placeholder':'Password'})

    submit = SubmitField("Login")

