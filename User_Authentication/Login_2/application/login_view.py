
from flask import Blueprint, render_template,flash,url_for,redirect
from flask_login import login_user,login_required,logout_user,current_user 
from .Form import Register, Login
from .crud import add_to_db,search_data_in_db 



login_auth = Blueprint('login_auth',__name__);


method = ['GET','POST']

@login_auth.route('/login',methods=method)
def login():
    form = Login()
    
    if form.validate_on_submit():
        email_user = form.email.data;
        password_user = form.password.data;

        user_exist = search_data_in_db(email_user,password_user)

        if user_exist[1]==1:
            login_user(user_exist[0]);
            flash("login successful")
            return redirect(url_for('login_auth.account'));
        else:
            flash(user_exist[0])

    return render_template('login.html',name='Login',xfor = form)


@login_auth.route('/acount')
@login_required
def account():
    X = current_user
    name = X.username;
    email = X.email 
    date = X.date_time
    return render_template('dashboard.html',name=name,em=email,dt=date);

@login_auth.route('/log-out')
@login_required  
def logout():
    logout_user()
    return redirect(url_for('login_auth.login'))

@login_auth.route('/signup',methods=method)
def sign_up():
    form = Register()

    if form.validate_on_submit():
    
        user_name = form.username.data 
        user_email = form.email.data
        user_password = form.password.data;
        user_password_con = form.pass_conform.data 

        if user_password_con == user_password:
            Add = add_to_db(name=user_name,email=user_email,password=user_password)

            if Add == 1:
                flash('your account has been added continue by login')
                return redirect(url_for('login_auth.login'))
            else:
                flash(Add)

    return render_template('signup.html',name="Register", xfor = form);
